import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vuetify'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import { createProvider } from './vue-apollo'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  provide: createProvider().provide(),
  store,
  render: h => h(App)
}).$mount('#app')
